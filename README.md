# Intro aux outils de gestion de k8s

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

nous allons introduire quelques outils de gestion de Kubernetes, à savoir :

Qu'est-ce que les outils de gestion de Kubernetes ?
Ainsi qu'une brève intro de certains de ces outils :
1. kubectl
2. kubeadm
3. minikube
4. helm
5. kompose
6. customize

# 1. Qu'est-ce que les outils de gestion de Kubernetes ?

Il existe une variété d'outils de gestion pour Kubernetes qui interagissent avec Kubernetes pour fournir des fonctionnalités supplémentaires. Essentiellement, ils facilitent l'utilisation de Kubernetes dans le monde réel. Une bonne connaissance de certains de ces outils permet de savoir ce qui est disponible pour simplifier la vie des utilisateurs de Kubernetes.

# 2. kubectl
Le premier outil abordé est kubectl. C'est probablement l'outil le plus utilisé par la plupart des gens. Kubectl est l'interface en ligne de commande officielle pour Kubernetes et est la méthode principale pour interagir avec Kubernetes, notamment lors de l'examen de l'administrateur certifié Kubernetes.

# 3. kubeadm
Un autre outil important est kubeadm. Kubeadm permet de créer rapidement et facilement des clusters Kubernetes. Une fois les quelques paquets logiciels requis pour Kubernetes installés, kubeadm simplifie grandement la configuration du plan de contrôle et la mise en place des nœuds de travail.

# 4. minikube
Minikube est un autre outil utile. Similaire à kubeadm, il permet de mettre en place un cluster Kubernetes rapidement, mais diffère en ce qu'il permet de lancer un cluster Kubernetes sur un seul serveur ou une seule machine. Minikube est idéal pour lancer rapidement un cluster Kubernetes à des fins de développement ou d'automatisation.

# 5. Helm
Helm est un outil très puissant pour Kubernetes. Il offre une solution de gestion de modèles et de paquets pour les objets Kubernetes. Helm permet de transformer les objets Kubernetes ou les applications exécutées dans un cluster en modèles, appelés charts, pour faciliter la gestion de configurations complexes de multiples objets Kubernetes. Des modèles partagés créés par d'autres utilisateurs peuvent également être téléchargés et utilisés.

# 6. Kompose
Kompose est particulièrement utile pour ceux qui passent de Docker à Kubernetes. Si des fichiers Docker Compose sont utilisés pour gérer des conteneurs, kompose peut automatiquement transformer ces fichiers en un ensemble d'objets Kubernetes. Pour ceux dont Docker fait partie du flux de travail ou qui passent de Docker à Kubernetes, kompose simplifie grandement ce processus.

# 7. Customize
Enfin, customize est un outil de gestion des configurations pour les objets Kubernetes. Semblable à Helm, il permet de partager et de réutiliser ces configurations modélisées pour les applications Kubernetes.

# Conclusion
Ceci couvre les outils de gestion de Kubernetes et fourni une briève introduit à kubectl, kubeadm, minikube, helm, kompose et customize. Ces outils facilitent grandement la prise en main et l'utilisation de Kubernetes.



# Reférences 

[Outils k8s :](https://kubernetes.io/docs/reference/tools/) https://kubernetes.io/docs/reference/tools/

[Personalisation de k8s :](https://kubernetes.io/blog/2018/05/29/introducing-kustomize-template-free-configuration-customization-for-kubernetes/) https://kubernetes.io/blog/2018/05/29/introducing-kustomize-template-free-configuration-customization-for-kubernetes/

